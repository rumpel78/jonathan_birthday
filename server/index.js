const express = require('express');
const path = require('path');
const cors = require('cors');
require('dotenv').config()

const key = process.env.KEY || 'ABC-123-DEF';

const app = express();

// CORS
const corsOption = {
    origin: true,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    credentials: true,
    exposedHeaders: [ 'x-auth-token' ],
  };
app.use(cors(corsOption));

// Serve the static files from the React app
app.use(express.static(path.join(__dirname, 'static')));

// An api endpoint that returns a short list of items
const endDate = (new Date("2018-10-21T08:00:00")).getTime();
app.get('/redeem', (req,res) => {
    const diff = endDate - Date.now()
    if (diff > 0) {
        throw ('Too early');
     } else {
        res.json({
            key, 
            link: 'https://store.steampowered.com/account/registerkey?key=' + key,
        });
    }
});

// Handles any requests that don't match the ones above
app.get('*', (req,res) =>{
    res.sendFile(path.join(__dirname+'/static/index.html'));
});

const port = process.env.PORT || 5000;
app.listen(port);

console.log('Using key ' + key);
console.log('App is listening on port ' + port);
