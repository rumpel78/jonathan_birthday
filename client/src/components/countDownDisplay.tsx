import { Grid } from '@material-ui/core';
import * as React from 'react';
import CircularProgressbar from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

interface ICountDownDisplayProps {
    days: number,
    hours: number,
    minutes: number,
    seconds: number,
    milliseconds: number,
}

export class CountDownDisplay extends React.Component<ICountDownDisplayProps> {
  public render() {
    const { days, hours, minutes, seconds } = this.props;

    const color = '#eeee';

    const style = {
      text: { fontSize: '14px', fill: color },
    };

    return (
      <div>
        <Grid container={true} direction="row" justify="center" alignItems="center" spacing={16} >
          <Grid item={true} xs={3}>
            <CircularProgressbar
              percentage={(days / 5) * 100}
              text={`${days.toString()} Tage`}
              styles={style}
            />
          </Grid>
          <Grid item={true} xs={3}>
            <CircularProgressbar
              percentage={(hours / 24) * 100}
              text={`${hours.toString()} Stunden`}
              styles={style}
            />
          </Grid>
          <Grid item={true} xs={3}>
            <CircularProgressbar
              percentage={(minutes / 60) * 100}
              text={`${minutes.toString()} Minuten`}
              styles={style}
            />
          </Grid>
          <Grid item={true} xs={3}>
            <CircularProgressbar
              percentage={(seconds / 60) * 100}
              text={`${seconds.toString()} Sekunden`}
              styles={style}
            />
          </Grid>       
        </Grid>
      </div>
    );
  }
}
