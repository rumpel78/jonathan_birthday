import * as React from 'react';
import './countDown.css';
import { CountDownDisplay } from './countDownDisplay';
import { CountDownTimer } from './countDownTimer';
import { Redeem } from './redeem';

export class CountDown extends React.Component {
    public render() {
        return (
            <div className="countdown">
                <CountDownTimer
                    // tslint:disable-next-line:jsx-no-lambda
                    renderWait={() => <span>Einen kurzen Moment noch....</span>}
                    // tslint:disable-next-line:jsx-no-lambda
                    render={(days, hours, minutes, seconds, milliseconds) => (
                        <CountDownDisplay
                        days={days}
                        hours={hours}
                        minutes={minutes}
                        seconds={seconds}
                        milliseconds={milliseconds} 
                        />
                    )}
                >
                    <Redeem />
                </CountDownTimer>
            </div>
        );
    }
}
