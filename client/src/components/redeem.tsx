import { Button, Fade, TextField } from '@material-ui/core';
import axios from 'axios';
import * as React from 'react';
import 'react-circular-progressbar/dist/styles.css';

interface IRedeemState {
    link?: string;
    key?: string;
    error: boolean;
}

interface IRedeemData {
    link: string;
    key: string;
}

export class Redeem extends React.Component<{}, IRedeemState> {
    public state = {
        error: false,
        key: undefined,
        link: undefined,
    }

    public async componentDidMount() {
        try {
            const response = await axios.get('/redeem');
            const data = response.data as IRedeemData;
            this.setState({...data});
        }
        catch (err) {
            this.setState({ error: true });
        }
    }

    public render() {

        if (this.state.error) {
            return (
                <div>
                    <h4>Ooops, etwas ist da wohl schiefgegangen - am besten rufst du mich an</h4>
                </div>
            );
        }

        if (!this.state.link) {
            return null;
        }

        return (
            <Fade in={true} timeout={8000}>
                <div>
                    <p>
                        <h1>Happy Birthday!</h1>
                        <h2>Alles Gute zum Geburtstag von Tante Sabrina, Onkel Thomas und der ganzen Familie!</h2>
                    </p>
                    <br /><br /><br />
                    <div>
                        <TextField 
                            label="Dein Steam-Key" 
                            value={this.state.key}
                            inputProps={{ readOnly: true }}
                            color="primary" 
                            variant="filled"
                        />
                    </div>
                    <br /><br />
                    <div>
                        <Button href={this.state.link} target="_blank" variant="contained" color="primary" >
                            Direkt Freischalten auf Steam!
                        </Button>
                    </div>
                    <br /><br />
                    <p>Solltest du Probleme beim Aktivieren haben, das Spiel bereits besitzen oder sonstige Probleme haben, ruf mich bitte an!</p>
                </div>
            </Fade>
        );
    }
}
