import * as React from 'react';

interface ICountDownTimerProps {
    renderWait: () => React.ReactNode;
    render: (days: number, hours: number, minutes:number, seconds: number, milliseconds: number) => React.ReactNode;
}

interface ICountDownTimerState {
    timer: number;
}

export class CountDownTimer extends React.Component<ICountDownTimerProps, ICountDownTimerState> {
    public state: ICountDownTimerState = {
        timer: 30 * 24 * 60 * 60 * 1000,
    };
      
    private intervalTimer: number;
    // private endDate = Date.now()+5000;
    private endDate = (new Date("2018-10-21T08:00:00")).getTime()

    public componentDidMount() {
        this.timerTick();
    }

    public componentWillUnmount() {
        clearTimeout(this.intervalTimer);   
    }   

    public render() {
        const wait = this.state.timer < 0 
        const done = this.state.timer < -5000 

        let secs = this.state.timer / 1000;
        let  mins =  secs / 60;
        let  hours = mins / 60;
        const  days = Math.floor(hours / 24);
        secs = Math.floor(secs % 60);
        mins = Math.floor(mins % 60);
        hours = Math.floor(hours % 24);

        if (wait && !done) {
            return this.props.renderWait();
        }

        if (done) {
            return this.props.children;
        }

        return this.props.render(days, hours, mins, secs, this.state.timer);
    }

    private  timerTick = async () => {
        this.setState(state => ({...state, timer: this.endDate - Date.now()}));
        this.intervalTimer = window.setTimeout(this.timerTick, 1000)
    };
}
