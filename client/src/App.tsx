import { Grid } from '@material-ui/core';
import * as React from 'react';
import './App.css';
import { CountDown } from './components/countDown';

interface IAppState {
  timer: number;
}

class App extends React.Component<{}, IAppState> {
  public render() {
    return (
      <div className="App">
        <Grid container={true} spacing={16} >
          <Grid item={true} xs={12}>
            <h1>Jonathan</h1>
            <h3>Hier erscheint dein Geburtstags-Geschenk:</h3>
          </Grid>
          <Grid item={true} xs={12}>
            <CountDown />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default App;
