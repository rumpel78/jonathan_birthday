import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

const theme = createMuiTheme();

// const color = '#eee';
// const theme = createMuiTheme({
//   palette: {
//     primary: {
//       main: color,
//     },
//     text: {
//       hint: color,
//       primary: color,
//       secondary: color,
//     },
//   },
// });

function Root() {
  return (
    <MuiThemeProvider theme={theme}>
      <App />
    </MuiThemeProvider>
  );
}

ReactDOM.render(
  <Root />,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
